// router.js
// TUTORIAL: https://appdividend.com/2019/02/14/node-express-image-upload-and-resize-tutorial-example/


const express = require('express');
const app = express();
const router = express.Router();
const upload = require('./uploadMiddleware');
const Resize = require('./Resize');
const path = require('path');

router.get('/', async function (req, res) {
  await res.render('index');
});

router.post('/post', upload.single('image'), async function (req, res) {
  console.log('POST')
  const imagePath = path.join(__dirname, '../public/img');
  const fileUpload = new Resize(imagePath);
  if (!req.file) {
    res.status(401).json({error: 'Please provide an image'});
  }
  const filename = await fileUpload.save(req.file.buffer);
  return res.status(200).json({ name: filename });
});

module.exports = router;