var express = require("express");
const bodyParser = require('body-parser');
var app = express();

const router = require('./router');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static('public'));
app.set('view engine', 'ejs');

app.use('/upload', router);

app.listen(3000, () => {
  console.log("Server running on port 3000");
})